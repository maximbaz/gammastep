# Croatian translation for redshift
# Copyright (c) 2011 Rosetta Contributors and Canonical Ltd 2011
# This file is distributed under the same license as the redshift package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: redshift\n"
"Report-Msgid-Bugs-To: https://github.com/jonls/redshift/issues\n"
"POT-Creation-Date: 2020-05-04 03:15-0700\n"
"PO-Revision-Date: 2015-02-23 06:54+0000\n"
"Last-Translator: Mario Dautović <mario.dautovic@yahoo.com>\n"
"Language-Team: Croatian <hr@li.org>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2018-05-21 01:04+0000\n"
"X-Generator: Launchpad (build 18658)\n"

#: data/applications/redshift.desktop.in:8
#: data/applications/redshift-gtk.desktop.in:8
msgid "redshift"
msgstr ""

#. TRANSLATORS: Name printed when period of day is unknown
#: src/redshift.c:106
msgid "None"
msgstr ""

#: src/redshift.c:107 src/redshift.c:1073
msgid "Daytime"
msgstr ""

#: src/redshift.c:108 src/redshift.c:1077
msgid "Night"
msgstr ""

#: src/redshift.c:109
msgid "Transition"
msgstr ""

#: src/redshift.c:196
#, c-format
msgid "Period: %s\n"
msgstr ""

#: src/redshift.c:199
#, c-format
msgid "Period: %s (%.2f%% day)\n"
msgstr ""

#. TRANSLATORS: Abbreviation for `north'
#: src/redshift.c:211
msgid "N"
msgstr ""

#. TRANSLATORS: Abbreviation for `south'
#: src/redshift.c:213
msgid "S"
msgstr ""

#. TRANSLATORS: Abbreviation for `east'
#: src/redshift.c:215
msgid "E"
msgstr ""

#. TRANSLATORS: Abbreviation for `west'
#: src/redshift.c:217
msgid "W"
msgstr ""

#. TRANSLATORS: Append degree symbols after %f if possible.
#. The string following each number is an abreviation for
#. north, source, east or west (N, S, E, W).
#: src/redshift.c:222
#, c-format
msgid "Location: %.2f %s, %.2f %s\n"
msgstr ""

#: src/redshift.c:296 src/redshift.c:387
#, c-format
msgid "Initialization of %s failed.\n"
msgstr "Učitavanje %s nije uspjelo.\n"

#: src/redshift.c:311 src/redshift.c:355 src/redshift.c:402 src/redshift.c:434
#, c-format
msgid "Failed to set %s option.\n"
msgstr "Postavljanje %s opcije nije uspjelo.\n"

#. TRANSLATORS: `help' must not be
#. translated.
#. TRANSLATORS: `help' must not be translated.
#: src/redshift.c:316 src/redshift.c:358
#, c-format
msgid "Try `-l %s:help' for more information.\n"
msgstr "Upišite `-l %s:help' za pojedinosti.\n"

#: src/redshift.c:344 src/redshift.c:424
#, c-format
msgid "Failed to parse option `%s'.\n"
msgstr ""

#: src/redshift.c:371
#, c-format
msgid "Failed to start provider %s.\n"
msgstr "Pogreška prilikom pokretanja pružatelja usluge %s.\n"

#. TRANSLATORS: `help' must not be
#. translated.
#: src/redshift.c:407
#, c-format
msgid "Try `-m %s:help' for more information.\n"
msgstr "Pokušajte `-m %s:help' za pojedinosti.\n"

#. TRANSLATORS: `help' must not be translated.
#: src/redshift.c:437
#, c-format
msgid "Try -m %s:help' for more information.\n"
msgstr "Pokušajte -m %s:help' za pojedinosti.\n"

#: src/redshift.c:449
#, c-format
msgid "Failed to start adjustment method %s.\n"
msgstr "Način podešavanja %s nije uspio.\n"

#: src/redshift.c:480
#, c-format
msgid "Latitude must be between %.1f and %.1f.\n"
msgstr "Zemljopisna širina mora biti između %.1f i %.1f.\n"

#: src/redshift.c:489
#, c-format
msgid "Longitude must be between %.1f and %.1f.\n"
msgstr "Zemljopisna dužina mora biti između %.1f i %.1f.\n"

#: src/redshift.c:516 src/redshift.c:535 src/redshift.c:667 src/redshift.c:1153
msgid "Unable to read system time.\n"
msgstr "Nije moguće pročitati vrijeme na sustavu.\n"

#: src/redshift.c:606
msgid "Waiting for initial location to become available...\n"
msgstr ""

#: src/redshift.c:612 src/redshift.c:804 src/redshift.c:819 src/redshift.c:1138
msgid "Unable to get location from provider.\n"
msgstr "Ne mogu dobiti lokaciju od pružatelja usluge.\n"

#: src/redshift.c:618 src/redshift.c:843
msgid "Invalid location returned from provider.\n"
msgstr ""

#: src/redshift.c:627 src/redshift.c:760 src/redshift.c:1188
#: src/redshift.c:1217
#, c-format
msgid "Color temperature: %uK\n"
msgstr "Temperatura boje: %uK\n"

#: src/redshift.c:628 src/redshift.c:765 src/redshift.c:1190
#, c-format
msgid "Brightness: %.2f\n"
msgstr "Jačina svjetla: %.2f\n"

#: src/redshift.c:657
#, c-format
msgid "Status: %s\n"
msgstr ""

#: src/redshift.c:658 src/redshift-gtk/statusicon.py:314
msgid "Disabled"
msgstr ""

#: src/redshift.c:658 src/redshift-gtk/statusicon.py:78
#: src/redshift-gtk/statusicon.py:314
msgid "Enabled"
msgstr ""

#: src/redshift.c:774 src/redshift.c:1199 src/redshift.c:1227
#: src/redshift.c:1248
msgid "Temperature adjustment failed.\n"
msgstr "Podešavanje temperature nije uspjelo.\n"

#: src/redshift.c:826
msgid ""
"Location is temporarily unavailable; Using previous location until it "
"becomes available...\n"
msgstr ""

#: src/redshift.c:933
msgid "Partitial time-configuration not supported!\n"
msgstr ""

#: src/redshift.c:941
msgid "Invalid dawn/dusk time configuration!\n"
msgstr ""

#: src/redshift.c:972
#, c-format
msgid "Trying location provider `%s'...\n"
msgstr ""

#: src/redshift.c:977
msgid "Trying next provider...\n"
msgstr "Pokušavam kod sljedećeg pružatelja usluge...\n"

#: src/redshift.c:983
#, c-format
msgid "Using provider `%s'.\n"
msgstr "Koristim pružatelja usluge `%s'.\n"

#: src/redshift.c:991
msgid "No more location providers to try.\n"
msgstr "Ne postoji više pružatelja usluge lociranja za probati.\n"

#: src/redshift.c:1000
#, c-format
msgid ""
"High transition elevation cannot be lower than the low transition "
"elevation.\n"
msgstr ""

#. TRANSLATORS: Append degree symbols if possible.
#: src/redshift.c:1007
#, c-format
msgid "Solar elevations: day above %.1f, night below %.1f\n"
msgstr ""

#: src/redshift.c:1015
#, c-format
msgid "Temperatures: %dK at day, %dK at night\n"
msgstr ""

#: src/redshift.c:1026 src/redshift.c:1037
#, c-format
msgid "Temperature must be between %uK and %uK.\n"
msgstr "Temperatura mora biti između %uK i %uK.\n"

#: src/redshift.c:1049
#, c-format
msgid "Brightness values must be between %.1f and %.1f.\n"
msgstr ""

#: src/redshift.c:1055
#, c-format
msgid "Brightness: %.2f:%.2f\n"
msgstr ""

#: src/redshift.c:1064
#, c-format
msgid "Gamma value must be between %.1f and %.1f.\n"
msgstr "Gama vrijednost mora biti između %.1f i %.1f.\n"

#. TRANSLATORS: The string in parenthesis is either
#. Daytime or Night (translated).
#: src/redshift.c:1072 src/redshift.c:1076
#, c-format
msgid "Gamma (%s): %.3f, %.3f, %.3f\n"
msgstr ""

#: src/redshift.c:1105
msgid "Trying next method...\n"
msgstr "Pokušavam sljedeći način...\n"

#: src/redshift.c:1110
#, c-format
msgid "Using method `%s'.\n"
msgstr "Koristim način `%s'.\n"

#: src/redshift.c:1117
msgid "No more methods to try.\n"
msgstr "Ne postoji više načina za probati.\n"

#: src/redshift.c:1131
msgid "Waiting for current location to become available...\n"
msgstr ""

#. TRANSLATORS: Append degree symbol if
#. possible.
#: src/redshift.c:1172
#, c-format
msgid "Solar elevation: %f\n"
msgstr "Sunčeva visina: %f\n"

#: src/redshift.c:1208 src/redshift.c:1235 src/redshift.c:1256
msgid "Press ctrl-c to stop...\n"
msgstr ""

#. TRANSLATORS: help output 1
#. LAT is latitude, LON is longitude,
#. DAY is temperature at daytime,
#. NIGHT is temperature at night
#. no-wrap
#: src/options.c:159
#, c-format
msgid "Usage: %s -l LAT:LON -t DAY:NIGHT [OPTIONS...]\n"
msgstr "Uporaba: %s -l DUŽ:ŠIR -t DNE:NOĆ [OPCIJE...]\n"

#. TRANSLATORS: help output 2
#. no-wrap
#: src/options.c:165
msgid "Set color temperature of display according to time of day.\n"
msgstr "Postaviti temperaturu boje zaslona ovisno o razdoblju dana.\n"

#. TRANSLATORS: help output 3
#. no-wrap
#: src/options.c:171
msgid ""
"  -h\t\tDisplay this help message\n"
"  -v\t\tVerbose output\n"
"  -V\t\tShow program version\n"
msgstr ""
"  -h\t\tPrikaži ovu pomoć\n"
"  -v\t\tOpširan ispis\n"
"  -V\t\tPrikaži inačicu programa\n"

#. TRANSLATORS: help output 4
#. `list' must not be translated
#. no-wrap
#: src/options.c:179
msgid ""
"  -b DAY:NIGHT\tScreen brightness to apply (between 0.1 and 1.0)\n"
"  -c FILE\tLoad settings from specified configuration file\n"
"  -g R:G:B\tAdditional gamma correction to apply\n"
"  -l LAT:LON\tYour current location\n"
"  -l PROVIDER\tSelect provider for automatic location updates\n"
"  \t\t(Type `list' to see available providers)\n"
"  -m METHOD\tMethod to use to set color temperature\n"
"  \t\t(Type `list' to see available methods)\n"
"  -o\t\tOne shot mode (do not continuously adjust color temperature)\n"
"  -O TEMP\tOne shot manual mode (set color temperature)\n"
"  -p\t\tPrint mode (only print parameters and exit)\n"
"  -P\t\tReset existing gamma ramps before applying new color effect\n"
"  -x\t\tReset mode (remove adjustment from screen)\n"
"  -r\t\tDisable fading between color temperatures\n"
"  -t DAY:NIGHT\tColor temperature to set at daytime/night\n"
msgstr ""

#. TRANSLATORS: help output 5
#: src/options.c:201
#, c-format
msgid ""
"The neutral temperature is %uK. Using this value will not change the color\n"
"temperature of the display. Setting the color temperature to a value higher\n"
"than this results in more blue light, and setting a lower value will result "
"in\n"
"more red light.\n"
msgstr ""

#. TRANSLATORS: help output 6
#: src/options.c:210
#, c-format
msgid ""
"Default values:\n"
"\n"
"  Daytime temperature: %uK\n"
"  Night temperature: %uK\n"
msgstr ""
"Zadane vrijednosti:\n"
"\n"
"  Dnevna temperatura: %uK\n"
"  Noćna temperatura: %uK\n"

#. TRANSLATORS: help output 7
#: src/options.c:218
#, c-format
msgid "Please report bugs to <%s>\n"
msgstr "Molimo vas greške prijavite na <%s>\n"

#: src/options.c:225
msgid "Available adjustment methods:\n"
msgstr "Dostupni načini prilagodbe\n"

#: src/options.c:231
msgid "Specify colon-separated options with `-m METHOD:OPTIONS'.\n"
msgstr ""
"Zadajte opcije s vrijednostima odvojenim dvotočkom `-m NAČIN:OPCIJA'.\n"

#. TRANSLATORS: `help' must not be translated.
#: src/options.c:234
msgid "Try `-m METHOD:help' for help.\n"
msgstr "Probajte `-m NAČIN:help' za pomoć.\n"

#: src/options.c:241
msgid "Available location providers:\n"
msgstr "Dostupni pružatelji lokacije\n"

#: src/options.c:247
msgid "Specify colon-separated options with`-l PROVIDER:OPTIONS'.\n"
msgstr ""
"Zadajte opcije s vrijednostima odvojenim dvotočkom `-l PRUŽATELJ_USLUGE:"
"OPCIJA'.\n"

#. TRANSLATORS: `help' must not be translated.
#: src/options.c:250
msgid "Try `-l PROVIDER:help' for help.\n"
msgstr "Probajte `-l PRUŽATELJ_USLUGE:help' za pomoć.\n"

#: src/options.c:351
msgid "Malformed gamma argument.\n"
msgstr "Netočan gama argument.\n"

#: src/options.c:352 src/options.c:463 src/options.c:481
msgid "Try `-h' for more information.\n"
msgstr "Pokušajte `-h' za pojedinosti.\n"

#: src/options.c:400 src/options.c:593
#, c-format
msgid "Unknown location provider `%s'.\n"
msgstr "Nepoznat pružatelj usluge lociranja `%s'.\n"

#. TRANSLATORS: This refers to the method
#. used to adjust colors e.g VidMode
#: src/options.c:431 src/options.c:583
#, c-format
msgid "Unknown adjustment method `%s'.\n"
msgstr "Nepoznata način podešavanja `%s'.\n"

#: src/options.c:462
msgid "Malformed temperature argument.\n"
msgstr "Netočna postavka temperature.\n"

#: src/options.c:553 src/options.c:565 src/options.c:574
msgid "Malformed gamma setting.\n"
msgstr "Netočne gama postavke.\n"

#: src/options.c:603
#, c-format
msgid "Malformed dawn-time setting `%s'.\n"
msgstr ""

#: src/options.c:613
#, c-format
msgid "Malformed dusk-time setting `%s'.\n"
msgstr ""

#: src/options.c:619
#, c-format
msgid "Unknown configuration setting `%s'.\n"
msgstr "Nepoznate postavke `%s'.\n"

#: src/config-ini.c:182
msgid "Malformed section header in config file.\n"
msgstr "Neispravno zaglavlje u datoteci s postavkama.\n"

#: src/config-ini.c:218
msgid "Malformed assignment in config file.\n"
msgstr "Neispravna dodjela vrijednosti u datoteci s postaavkama.\n"

#: src/config-ini.c:229
msgid "Assignment outside section in config file.\n"
msgstr "Dodjela vrijednosti izvan okvira u datoteci s postavkama.\n"

#: src/gamma-drm.c:101
#, c-format
msgid "Failed to open DRM device: %s\n"
msgstr ""

#: src/gamma-drm.c:109
#, c-format
msgid "Failed to get DRM mode resources\n"
msgstr ""

#: src/gamma-drm.c:119 src/gamma-randr.c:388
#, c-format
msgid "CRTC %d does not exist. "
msgstr "CRTC %d ne postoji. "

#: src/gamma-drm.c:122 src/gamma-randr.c:391
#, c-format
msgid "Valid CRTCs are [0-%d].\n"
msgstr "Valjani CRTCs su [0-%d].\n"

#: src/gamma-drm.c:125 src/gamma-randr.c:394
#, c-format
msgid "Only CRTC 0 exists.\n"
msgstr "Postoji samo CRTC 0.\n"

#: src/gamma-drm.c:163
#, c-format
msgid "CRTC %i lost, skipping\n"
msgstr ""

#: src/gamma-drm.c:169
#, c-format
msgid ""
"Could not get gamma ramp size for CRTC %i\n"
"on graphics card %i, ignoring device.\n"
msgstr ""

#: src/gamma-drm.c:182
#, c-format
msgid ""
"DRM could not read gamma ramps on CRTC %i on\n"
"graphics card %i, ignoring device.\n"
msgstr ""

#: src/gamma-drm.c:246
msgid "Adjust gamma ramps with Direct Rendering Manager.\n"
msgstr ""

#. TRANSLATORS: DRM help output
#. left column must not be translated
#: src/gamma-drm.c:251
msgid ""
"  card=N\tGraphics card to apply adjustments to\n"
"  crtc=N\tCRTC to apply adjustments to\n"
msgstr ""

#: src/gamma-drm.c:264
#, c-format
msgid "CRTC must be a non-negative integer\n"
msgstr ""

#: src/gamma-drm.c:268 src/gamma-randr.c:373 src/gamma-vidmode.c:165
#: src/gamma-dummy.c:71 src/location-geoclue2.c:412 src/location-manual.c:110
#, c-format
msgid "Unknown method parameter: `%s'.\n"
msgstr "Neispravan parametar `%s' za metodu.\n"

#: src/gamma-wl.c:84
#, c-format
msgid ""
"Fatal: redshift was not authorized to bind the "
"'zwlr_gamma_control_manager_v1' interface.\n"
msgstr ""

#: src/gamma-wl.c:104
#, fuzzy, c-format
msgid "Failed to allcate memory\n"
msgstr "Način podešavanja %s nije uspio.\n"

#: src/gamma-wl.c:138
#, c-format
msgid "The zwlr_gamma_control_manager_v1 was removed\n"
msgstr ""

#: src/gamma-wl.c:191
msgid "Could not connect to wayland display, exiting.\n"
msgstr ""

#: src/gamma-wl.c:232
#, c-format
msgid "Ignoring error on wayland connection while waiting to disconnect: %d\n"
msgstr ""

#: src/gamma-wl.c:261
#, fuzzy
msgid "Adjust gamma ramps with a Wayland compositor.\n"
msgstr "Prilagodi gama krivulju s Windows GDI.\n"

#: src/gamma-wl.c:296
#, c-format
msgid "The Wayland connection experienced a fatal error: %d\n"
msgstr ""

#: src/gamma-randr.c:98 src/gamma-randr.c:157 src/gamma-randr.c:196
#: src/gamma-randr.c:222 src/gamma-randr.c:279 src/gamma-randr.c:439
#, c-format
msgid "`%s' returned error %d\n"
msgstr "`%s' je vratio grešku %d\n"

#: src/gamma-randr.c:107
#, c-format
msgid "Unsupported RANDR version (%u.%u)\n"
msgstr "Nepodržana RANDR inačica (%u.%u)\n"

#: src/gamma-randr.c:142
#, c-format
msgid "Screen %i could not be found.\n"
msgstr "Zaslon %i nije moguće pronaći.\n"

#: src/gamma-randr.c:208 src/gamma-vidmode.c:100
#, c-format
msgid "Gamma ramp size too small: %i\n"
msgstr "Veličina gama krivulje je premala: %i\n"

#: src/gamma-randr.c:281
#, c-format
msgid "Unable to restore CRTC %i\n"
msgstr "Nije moguće povratiti CRTC %i\n"

#: src/gamma-randr.c:305
msgid "Adjust gamma ramps with the X RANDR extension.\n"
msgstr "Prilagodi gama krivulju sa X RANDR proširenjem.\n"

#. TRANSLATORS: RANDR help output
#. left column must not be translated
#: src/gamma-randr.c:310
msgid ""
"  screen=N\t\tX screen to apply adjustments to\n"
"  crtc=N\tList of comma separated CRTCs to apply adjustments to\n"
msgstr ""

#: src/gamma-randr.c:332
#, c-format
msgid "Unable to read screen number: `%s'.\n"
msgstr ""

#: src/gamma-randr.c:368 src/gamma-vidmode.c:160
#, c-format
msgid ""
"Parameter `%s` is now always on;  Use the `%s` command-line option to "
"disable.\n"
msgstr ""

#: src/gamma-vidmode.c:65 src/gamma-vidmode.c:85 src/gamma-vidmode.c:94
#: src/gamma-vidmode.c:121 src/gamma-vidmode.c:184 src/gamma-vidmode.c:229
#, c-format
msgid "X request failed: %s\n"
msgstr "X zahtjev nije uspio: %s\n"

#: src/gamma-vidmode.c:144
msgid "Adjust gamma ramps with the X VidMode extension.\n"
msgstr "Prilagodi gama krivulju sa X VidMode proširenjem.\n"

#. TRANSLATORS: VidMode help output
#. left column must not be translated
#: src/gamma-vidmode.c:149
msgid "  screen=N\t\tX screen to apply adjustments to\n"
msgstr ""

#: src/gamma-dummy.c:47
msgid ""
"WARNING: Using dummy gamma method! Display will not be affected by this "
"gamma method.\n"
msgstr ""

#: src/gamma-dummy.c:64
msgid ""
"Does not affect the display but prints the color temperature to the "
"terminal.\n"
msgstr ""

#: src/gamma-dummy.c:79
#, c-format
msgid "Temperature: %i\n"
msgstr ""

#: src/location-geoclue2.c:63
msgid ""
"Access to the current location was denied by GeoClue!\n"
"Make sure that location services are enabled and that Redshift is permitted\n"
"to use location services. See https://github.com/jonls/redshift#faq for "
"more\n"
"information.\n"
msgstr ""

#: src/location-geoclue2.c:111
#, c-format
msgid "Unable to obtain location: %s.\n"
msgstr ""

#: src/location-geoclue2.c:154
#, c-format
msgid "Unable to obtain GeoClue Manager: %s.\n"
msgstr ""

#: src/location-geoclue2.c:170
#, c-format
msgid "Unable to obtain GeoClue client path: %s.\n"
msgstr ""

#: src/location-geoclue2.c:192
#, c-format
msgid "Unable to obtain GeoClue Client: %s.\n"
msgstr ""

#: src/location-geoclue2.c:233
#, c-format
msgid "Unable to set distance threshold: %s.\n"
msgstr ""

#: src/location-geoclue2.c:257
#, c-format
msgid "Unable to start GeoClue client: %s.\n"
msgstr ""

#: src/location-geoclue2.c:369
msgid "Failed to start GeoClue2 provider!\n"
msgstr ""

#: src/location-geoclue2.c:403
msgid "Use the location as discovered by a GeoClue2 provider.\n"
msgstr ""

#: src/location-manual.c:63
msgid "Latitude and longitude must be set.\n"
msgstr "Zemljopisna širina i dužina moraju biti postavljene.\n"

#: src/location-manual.c:79
msgid "Specify location manually.\n"
msgstr "Ručno određivanje lokacije.\n"

#. TRANSLATORS: Manual location help output
#. left column must not be translated
#: src/location-manual.c:84
msgid ""
"  lat=N\t\tLatitude\n"
"  lon=N\t\tLongitude\n"
msgstr ""
"  lat=N\t\tŠirina\n"
"  lon=N\t\tDužina\n"

#: src/location-manual.c:87
msgid ""
"Both values are expected to be floating point numbers,\n"
"negative values representing west / south, respectively.\n"
msgstr ""

#: src/location-manual.c:101
msgid "Malformed argument.\n"
msgstr "Neispravan argument.\n"

#: src/redshift-gtk/statusicon.py:83
msgid "Suspend for"
msgstr ""

#: src/redshift-gtk/statusicon.py:85
msgid "30 minutes"
msgstr ""

#: src/redshift-gtk/statusicon.py:86
msgid "1 hour"
msgstr ""

#: src/redshift-gtk/statusicon.py:87
msgid "2 hours"
msgstr ""

#: src/redshift-gtk/statusicon.py:88
msgid "4 hours"
msgstr ""

#: src/redshift-gtk/statusicon.py:89
msgid "8 hours"
msgstr ""

#: src/redshift-gtk/statusicon.py:98
msgid "Autostart"
msgstr "Automatsko pokretanje"

#: src/redshift-gtk/statusicon.py:110 src/redshift-gtk/statusicon.py:120
msgid "Info"
msgstr ""

#: src/redshift-gtk/statusicon.py:115
msgid "Quit"
msgstr ""

#: src/redshift-gtk/statusicon.py:153
msgid "Close"
msgstr ""

#: src/redshift-gtk/statusicon.py:313
msgid "<b>Status:</b> {}"
msgstr ""

#: src/redshift-gtk/statusicon.py:319 src/redshift-gtk/statusicon.py:337
msgid "Color temperature"
msgstr ""

#: src/redshift-gtk/statusicon.py:325 src/redshift-gtk/statusicon.py:338
msgid "Period"
msgstr ""

#: src/redshift-gtk/statusicon.py:331
msgid "Location"
msgstr ""

#: src/redshift-gtk/statusicon.py:362
msgid "Please run `redshift -h` for help output."
msgstr ""

#~ msgid "Unable to save current gamma ramp.\n"
#~ msgstr "Nije moguće spremiti trenutnu gama krivulju.\n"

#~ msgid "Unable to open device context.\n"
#~ msgstr "Nemoguće je otvoriti sadržaj uređaja.\n"

#~ msgid "Display device does not support gamma ramps.\n"
#~ msgstr "Zaslon ne podržava gama krivulju.\n"

#~ msgid "Unable to restore gamma ramps.\n"
#~ msgstr "Nije moguće povratiti gama krivulju.\n"

#~ msgid "Unable to set gamma ramps.\n"
#~ msgstr "Nije moguće postaviti gama krivulju.\n"
