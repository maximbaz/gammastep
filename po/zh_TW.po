# Chinese (Traditional) translation for redshift
# Copyright (c) 2018 Rosetta Contributors and Canonical Ltd 2018
# This file is distributed under the same license as the redshift package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: redshift\n"
"Report-Msgid-Bugs-To: https://github.com/jonls/redshift/issues\n"
"POT-Creation-Date: 2020-05-04 03:15-0700\n"
"PO-Revision-Date: 2018-04-28 15:30+0000\n"
"Last-Translator: BigELK176 ≡ <BigELK176@gmail.com>\n"
"Language-Team: Chinese (Traditional) <zh_TW@li.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2018-05-21 01:04+0000\n"
"X-Generator: Launchpad (build 18658)\n"

#: data/applications/redshift.desktop.in:8
#: data/applications/redshift-gtk.desktop.in:8
#, fuzzy
msgid "redshift"
msgstr "Redshift"

#. TRANSLATORS: Name printed when period of day is unknown
#: src/redshift.c:106
msgid "None"
msgstr "無"

#: src/redshift.c:107 src/redshift.c:1073
msgid "Daytime"
msgstr "日間"

#: src/redshift.c:108 src/redshift.c:1077
msgid "Night"
msgstr "夜間"

#: src/redshift.c:109
msgid "Transition"
msgstr "轉換"

#: src/redshift.c:196
#, c-format
msgid "Period: %s\n"
msgstr "期間： %s\n"

#: src/redshift.c:199
#, c-format
msgid "Period: %s (%.2f%% day)\n"
msgstr "期間： %s (%.2f%% day)\n"

#. TRANSLATORS: Abbreviation for `north'
#: src/redshift.c:211
msgid "N"
msgstr "北"

#. TRANSLATORS: Abbreviation for `south'
#: src/redshift.c:213
msgid "S"
msgstr "南"

#. TRANSLATORS: Abbreviation for `east'
#: src/redshift.c:215
msgid "E"
msgstr "東"

#. TRANSLATORS: Abbreviation for `west'
#: src/redshift.c:217
msgid "W"
msgstr "西"

#. TRANSLATORS: Append degree symbols after %f if possible.
#. The string following each number is an abreviation for
#. north, source, east or west (N, S, E, W).
#: src/redshift.c:222
#, c-format
msgid "Location: %.2f %s, %.2f %s\n"
msgstr "位置： %.2f %s, %.2f %s\n"

#: src/redshift.c:296 src/redshift.c:387
#, c-format
msgid "Initialization of %s failed.\n"
msgstr "針對 %s 初始化失敗。\n"

#: src/redshift.c:311 src/redshift.c:355 src/redshift.c:402 src/redshift.c:434
#, c-format
msgid "Failed to set %s option.\n"
msgstr "設定 %s 選項失敗。\n"

#. TRANSLATORS: `help' must not be
#. translated.
#. TRANSLATORS: `help' must not be translated.
#: src/redshift.c:316 src/redshift.c:358
#, c-format
msgid "Try `-l %s:help' for more information.\n"
msgstr "試試 `-l %s:help' 取得更多資訊。\n"

#: src/redshift.c:344 src/redshift.c:424
#, c-format
msgid "Failed to parse option `%s'.\n"
msgstr "解析 '%s' 選項失敗\n"

#: src/redshift.c:371
#, c-format
msgid "Failed to start provider %s.\n"
msgstr "啟動供應者 %s 失敗。\n"

#. TRANSLATORS: `help' must not be
#. translated.
#: src/redshift.c:407
#, c-format
msgid "Try `-m %s:help' for more information.\n"
msgstr "試試 `-m %s:help' 取得更多資訊。\n"

#. TRANSLATORS: `help' must not be translated.
#: src/redshift.c:437
#, c-format
msgid "Try -m %s:help' for more information.\n"
msgstr "試試 -m %s:help' 取得更多資訊。\n"

#: src/redshift.c:449
#, c-format
msgid "Failed to start adjustment method %s.\n"
msgstr "啟動調整方法 %s 失敗。\n"

#: src/redshift.c:480
#, c-format
msgid "Latitude must be between %.1f and %.1f.\n"
msgstr "緯度必須在 %.1f 及 %.1f 之間。\n"

#: src/redshift.c:489
#, c-format
msgid "Longitude must be between %.1f and %.1f.\n"
msgstr "經度必須在 %.1f 及 %.1f 之間。\n"

#: src/redshift.c:516 src/redshift.c:535 src/redshift.c:667 src/redshift.c:1153
msgid "Unable to read system time.\n"
msgstr "無法讀取系統時間。\n"

#: src/redshift.c:606
msgid "Waiting for initial location to become available...\n"
msgstr "等待初始化位置變成可以使用…\n"

#: src/redshift.c:612 src/redshift.c:804 src/redshift.c:819 src/redshift.c:1138
msgid "Unable to get location from provider.\n"
msgstr "無法由供應者取得位置資訊。\n"

#: src/redshift.c:618 src/redshift.c:843
msgid "Invalid location returned from provider.\n"
msgstr "供應者回覆不合用的位置資訊。\n"

#: src/redshift.c:627 src/redshift.c:760 src/redshift.c:1188
#: src/redshift.c:1217
#, c-format
msgid "Color temperature: %uK\n"
msgstr "色溫：%uK\n"

#: src/redshift.c:628 src/redshift.c:765 src/redshift.c:1190
#, c-format
msgid "Brightness: %.2f\n"
msgstr "亮度： %.2f\n"

#: src/redshift.c:657
#, c-format
msgid "Status: %s\n"
msgstr "狀態： %s\n"

#: src/redshift.c:658 src/redshift-gtk/statusicon.py:314
msgid "Disabled"
msgstr "已停用"

#: src/redshift.c:658 src/redshift-gtk/statusicon.py:78
#: src/redshift-gtk/statusicon.py:314
msgid "Enabled"
msgstr "已啟用"

#: src/redshift.c:774 src/redshift.c:1199 src/redshift.c:1227
#: src/redshift.c:1248
msgid "Temperature adjustment failed.\n"
msgstr "色溫調整失敗。\n"

#: src/redshift.c:826
msgid ""
"Location is temporarily unavailable; Using previous location until it "
"becomes available...\n"
msgstr "位置資訊暫時無法使用；使用之前的位置資訊，直到可用提供…\n"

#: src/redshift.c:933
msgid "Partitial time-configuration not supported!\n"
msgstr "有部分的時間配置尚未支援！\n"

#: src/redshift.c:941
msgid "Invalid dawn/dusk time configuration!\n"
msgstr "無效的日出/日落時間配置！\n"

#: src/redshift.c:972
#, c-format
msgid "Trying location provider `%s'...\n"
msgstr "試用位置資訊供應用者 `%s' ...\n"

#: src/redshift.c:977
msgid "Trying next provider...\n"
msgstr "試用下一個供應用者 `%s' ...\n"

#: src/redshift.c:983
#, c-format
msgid "Using provider `%s'.\n"
msgstr "使用供應用者 `%s' 。\n"

#: src/redshift.c:991
msgid "No more location providers to try.\n"
msgstr "沒有其它位置資訊供應用者可供試用。\n"

#: src/redshift.c:1000
#, c-format
msgid ""
"High transition elevation cannot be lower than the low transition "
"elevation.\n"
msgstr "高過渡海拔不可低於低過渡海拔。\n"

#. TRANSLATORS: Append degree symbols if possible.
#: src/redshift.c:1007
#, c-format
msgid "Solar elevations: day above %.1f, night below %.1f\n"
msgstr "太陽海拔：日間高於 %.1f ．夜間低於 %.1f 。\n"

#: src/redshift.c:1015
#, c-format
msgid "Temperatures: %dK at day, %dK at night\n"
msgstr "溫度：日間為  %dK ．夜間為 %dK\n"

#: src/redshift.c:1026 src/redshift.c:1037
#, c-format
msgid "Temperature must be between %uK and %uK.\n"
msgstr "溫度必須介於 %uK 及 %uK 之間。\n"

#: src/redshift.c:1049
#, c-format
msgid "Brightness values must be between %.1f and %.1f.\n"
msgstr "高度數值必須介於 %.1f 及 %.1f 之間。\n"

#: src/redshift.c:1055
#, c-format
msgid "Brightness: %.2f:%.2f\n"
msgstr "亮度： %.2f:%.2f\n"

#: src/redshift.c:1064
#, c-format
msgid "Gamma value must be between %.1f and %.1f.\n"
msgstr "伽瑪數值必須介於 %.1f 及 %.1f 之間。\n"

#. TRANSLATORS: The string in parenthesis is either
#. Daytime or Night (translated).
#: src/redshift.c:1072 src/redshift.c:1076
#, c-format
msgid "Gamma (%s): %.3f, %.3f, %.3f\n"
msgstr "伽瑪 (%s): %.3f, %.3f, %.3f\n"

#: src/redshift.c:1105
msgid "Trying next method...\n"
msgstr "試用下一個方法…\n"

#: src/redshift.c:1110
#, c-format
msgid "Using method `%s'.\n"
msgstr "使用方法`%s'。\n"

#: src/redshift.c:1117
msgid "No more methods to try.\n"
msgstr "沒有其它方法可供試用。\n"

#: src/redshift.c:1131
msgid "Waiting for current location to become available...\n"
msgstr "等待目前位置資訊可供使用…\n"

#. TRANSLATORS: Append degree symbol if
#. possible.
#: src/redshift.c:1172
#, c-format
msgid "Solar elevation: %f\n"
msgstr "太陽海拔： %f\n"

#: src/redshift.c:1208 src/redshift.c:1235 src/redshift.c:1256
msgid "Press ctrl-c to stop...\n"
msgstr "按 ctrl-c 停止 ...\n"

#. TRANSLATORS: help output 1
#. LAT is latitude, LON is longitude,
#. DAY is temperature at daytime,
#. NIGHT is temperature at night
#. no-wrap
#: src/options.c:159
#, c-format
msgid "Usage: %s -l LAT:LON -t DAY:NIGHT [OPTIONS...]\n"
msgstr "用法: %s -l LAT:LON -t DAY:NIGHT [選項...]\n"

#. TRANSLATORS: help output 2
#. no-wrap
#: src/options.c:165
msgid "Set color temperature of display according to time of day.\n"
msgstr "根據日間時間來設定顯示器的色溫\n"

#. TRANSLATORS: help output 3
#. no-wrap
#: src/options.c:171
msgid ""
"  -h\t\tDisplay this help message\n"
"  -v\t\tVerbose output\n"
"  -V\t\tShow program version\n"
msgstr ""
"  -h\t\t顯示說明訊息\n"
"  -v\t\t細節內容輸出\n"
"  -V\t\t顯示軟體版本\n"

#. TRANSLATORS: help output 4
#. `list' must not be translated
#. no-wrap
#: src/options.c:179
msgid ""
"  -b DAY:NIGHT\tScreen brightness to apply (between 0.1 and 1.0)\n"
"  -c FILE\tLoad settings from specified configuration file\n"
"  -g R:G:B\tAdditional gamma correction to apply\n"
"  -l LAT:LON\tYour current location\n"
"  -l PROVIDER\tSelect provider for automatic location updates\n"
"  \t\t(Type `list' to see available providers)\n"
"  -m METHOD\tMethod to use to set color temperature\n"
"  \t\t(Type `list' to see available methods)\n"
"  -o\t\tOne shot mode (do not continuously adjust color temperature)\n"
"  -O TEMP\tOne shot manual mode (set color temperature)\n"
"  -p\t\tPrint mode (only print parameters and exit)\n"
"  -P\t\tReset existing gamma ramps before applying new color effect\n"
"  -x\t\tReset mode (remove adjustment from screen)\n"
"  -r\t\tDisable fading between color temperatures\n"
"  -t DAY:NIGHT\tColor temperature to set at daytime/night\n"
msgstr ""
"  -b DAY:NIGHT\t螢幕亮度套用（介於 0.1 及 1.0）\n"
"  -c FILE\t從指定的設置檔案來載入設置\n"
"  -g R:G:B\t額外的伽瑪校正設置\n"
"  -l LAT:LON \t你目前的位置\n"
"  -l PROVIDER\t選擇自動位置更新的提供者\n"
"  \t\t輸入 `list' 來查看可用的提供者）\n"
"  -m METHOD\t用來設置色溫的方法\n"
"  \t\t(輸入 `list' 來查看可用的方法）\n"
"  -o\t\t單觸發模式（不要持續地調整色溫）\n"
"  -O TEMP\t單觸發手動模式（設置色溫）\n"
"  -p\t\t列印模式（僅列列印參數值並退出）\n"
"  -P\t\t在套用新的顏色效果前要重置咖瑪斜面\n"
"  -x\t\t重置模式（從螢幕上移除調整）\n"
"  -r\t\t停用色溫之間的淡化轉化\n"
"  -t DAY:NIGHT\t設置日間/夜間的色溫\n"

#. TRANSLATORS: help output 5
#: src/options.c:201
#, c-format
msgid ""
"The neutral temperature is %uK. Using this value will not change the color\n"
"temperature of the display. Setting the color temperature to a value higher\n"
"than this results in more blue light, and setting a lower value will result "
"in\n"
"more red light.\n"
msgstr ""
"中性色溫為 %uK 。\n"
"使用該值不會改變顯示器的色溫。\n"
"設置色溫值高於它，會導致更多的藍光。\n"
"設置色溫值低於它，會導致更多的紅光。\n"

#. TRANSLATORS: help output 6
#: src/options.c:210
#, c-format
msgid ""
"Default values:\n"
"\n"
"  Daytime temperature: %uK\n"
"  Night temperature: %uK\n"
msgstr ""
"預設值：\n"
"\n"
"  日間色溫： %uK\n"
"  夜間色溫： %uK\n"

#. TRANSLATORS: help output 7
#: src/options.c:218
#, c-format
msgid "Please report bugs to <%s>\n"
msgstr "請回報 bug 至  <%s>\n"

#: src/options.c:225
msgid "Available adjustment methods:\n"
msgstr "可供使用的調整方法：\n"

#: src/options.c:231
msgid "Specify colon-separated options with `-m METHOD:OPTIONS'.\n"
msgstr "用冒號分隔選項  `-m METHOD:OPTIONS'\n"

#. TRANSLATORS: `help' must not be translated.
#: src/options.c:234
msgid "Try `-m METHOD:help' for help.\n"
msgstr "試用 `-m METHOD:help' 取得說明。\n"

#: src/options.c:241
msgid "Available location providers:\n"
msgstr "可供使用的位置資訊供應者：\n"

#: src/options.c:247
msgid "Specify colon-separated options with`-l PROVIDER:OPTIONS'.\n"
msgstr "用冒號來分別選項 `-l PROVIDER:OPTIONS'。\n"

#. TRANSLATORS: `help' must not be translated.
#: src/options.c:250
msgid "Try `-l PROVIDER:help' for help.\n"
msgstr "試用 `-l PROVIDER:help' 來獲得說明。\n"

#: src/options.c:351
msgid "Malformed gamma argument.\n"
msgstr "異常的咖瑪參數。\n"

#: src/options.c:352 src/options.c:463 src/options.c:481
msgid "Try `-h' for more information.\n"
msgstr "試用 `-h' 獲得更多資訊。\n"

#: src/options.c:400 src/options.c:593
#, c-format
msgid "Unknown location provider `%s'.\n"
msgstr "不明的位置資訊供應者 `%s' 。\n"

#. TRANSLATORS: This refers to the method
#. used to adjust colors e.g VidMode
#: src/options.c:431 src/options.c:583
#, c-format
msgid "Unknown adjustment method `%s'.\n"
msgstr "不明的調整方法 `%s'。\n"

#: src/options.c:462
msgid "Malformed temperature argument.\n"
msgstr "異常的溫度參數。\n"

#: src/options.c:553 src/options.c:565 src/options.c:574
msgid "Malformed gamma setting.\n"
msgstr "髒常的咖瑪設定。\n"

#: src/options.c:603
#, c-format
msgid "Malformed dawn-time setting `%s'.\n"
msgstr "異常的日出時間設定 `%s'.\n"

#: src/options.c:613
#, c-format
msgid "Malformed dusk-time setting `%s'.\n"
msgstr "異常的日落時間設定 `%s'.\n"

#: src/options.c:619
#, c-format
msgid "Unknown configuration setting `%s'.\n"
msgstr "不明的設置設定 `%s'.\n"

#: src/config-ini.c:182
msgid "Malformed section header in config file.\n"
msgstr "設置檔案中異常分配存在。\n"

#: src/config-ini.c:218
msgid "Malformed assignment in config file.\n"
msgstr "設置檔案中分配異常。\n"

#: src/config-ini.c:229
msgid "Assignment outside section in config file.\n"
msgstr "設置檔案中分配外部段落。\n"

#: src/gamma-drm.c:101
#, c-format
msgid "Failed to open DRM device: %s\n"
msgstr "開啟 DRM 設備失敗： %s\n"

#: src/gamma-drm.c:109
#, c-format
msgid "Failed to get DRM mode resources\n"
msgstr "取得 DRM 模式資源失敗\n"

#: src/gamma-drm.c:119 src/gamma-randr.c:388
#, c-format
msgid "CRTC %d does not exist. "
msgstr "CRTC %d 不存在。 "

#: src/gamma-drm.c:122 src/gamma-randr.c:391
#, c-format
msgid "Valid CRTCs are [0-%d].\n"
msgstr "有效的 CRTCs 為 [0-%d]。\n"

#: src/gamma-drm.c:125 src/gamma-randr.c:394
#, c-format
msgid "Only CRTC 0 exists.\n"
msgstr "僅有 CRTC 0 存在。\n"

#: src/gamma-drm.c:163
#, c-format
msgid "CRTC %i lost, skipping\n"
msgstr "CRTC %i 遺失，跳過中\n"

#: src/gamma-drm.c:169
#, c-format
msgid ""
"Could not get gamma ramp size for CRTC %i\n"
"on graphics card %i, ignoring device.\n"
msgstr ""
"在顯示卡 %i 不能取得用於 CRTC %i 的咖瑪斜面大小，\n"
"忽略設備中。\n"

#: src/gamma-drm.c:182
#, c-format
msgid ""
"DRM could not read gamma ramps on CRTC %i on\n"
"graphics card %i, ignoring device.\n"
msgstr ""
"DRM 不能讀到咖瑪斜面於 CRTC %i on\n"
"顯示卡 %i , 忽略設備。\n"

#: src/gamma-drm.c:246
msgid "Adjust gamma ramps with Direct Rendering Manager.\n"
msgstr "用 Direct Rendering Manager 調整咖瑪斜面。\n"

#. TRANSLATORS: DRM help output
#. left column must not be translated
#: src/gamma-drm.c:251
msgid ""
"  card=N\tGraphics card to apply adjustments to\n"
"  crtc=N\tCRTC to apply adjustments to\n"
msgstr ""
"  card=N\t顯示卡去套用調整至\n"
"  crtc=N\tCRTC 去套用調整至\n"

#: src/gamma-drm.c:264
#, c-format
msgid "CRTC must be a non-negative integer\n"
msgstr "CRTC 必須為非負整數\n"

#: src/gamma-drm.c:268 src/gamma-randr.c:373 src/gamma-vidmode.c:165
#: src/gamma-dummy.c:71 src/location-geoclue2.c:412 src/location-manual.c:110
#, c-format
msgid "Unknown method parameter: `%s'.\n"
msgstr "不明的方法參數： `%s'.\n"

#: src/gamma-wl.c:84
#, c-format
msgid ""
"Fatal: redshift was not authorized to bind the "
"'zwlr_gamma_control_manager_v1' interface.\n"
msgstr ""

#: src/gamma-wl.c:104
#, fuzzy, c-format
msgid "Failed to allcate memory\n"
msgstr "啟動調整方法 %s 失敗。\n"

#: src/gamma-wl.c:138
#, c-format
msgid "The zwlr_gamma_control_manager_v1 was removed\n"
msgstr ""

#: src/gamma-wl.c:191
msgid "Could not connect to wayland display, exiting.\n"
msgstr ""

#: src/gamma-wl.c:232
#, c-format
msgid "Ignoring error on wayland connection while waiting to disconnect: %d\n"
msgstr ""

#: src/gamma-wl.c:261
#, fuzzy
msgid "Adjust gamma ramps with a Wayland compositor.\n"
msgstr "用 Windows GDI 調整咖瑪斜面。\n"

#: src/gamma-wl.c:296
#, c-format
msgid "The Wayland connection experienced a fatal error: %d\n"
msgstr ""

#: src/gamma-randr.c:98 src/gamma-randr.c:157 src/gamma-randr.c:196
#: src/gamma-randr.c:222 src/gamma-randr.c:279 src/gamma-randr.c:439
#, c-format
msgid "`%s' returned error %d\n"
msgstr "`%s' 擲回錯誤 %d\n"

#: src/gamma-randr.c:107
#, c-format
msgid "Unsupported RANDR version (%u.%u)\n"
msgstr "不支援的 RANDR 版本 (%u.%u)\n"

#: src/gamma-randr.c:142
#, c-format
msgid "Screen %i could not be found.\n"
msgstr "螢幕 %i 未被找到。\n"

#: src/gamma-randr.c:208 src/gamma-vidmode.c:100
#, c-format
msgid "Gamma ramp size too small: %i\n"
msgstr "咖瑪斜面值太小： %i\n"

#: src/gamma-randr.c:281
#, c-format
msgid "Unable to restore CRTC %i\n"
msgstr "無法存入 CRTC %i\n"

#: src/gamma-randr.c:305
msgid "Adjust gamma ramps with the X RANDR extension.\n"
msgstr "使用 X RANDR 擴充來調整咖瑪斜面。\n"

#. TRANSLATORS: RANDR help output
#. left column must not be translated
#: src/gamma-randr.c:310
msgid ""
"  screen=N\t\tX screen to apply adjustments to\n"
"  crtc=N\tList of comma separated CRTCs to apply adjustments to\n"
msgstr ""
"  螢幕=N\t\tX 螢幕去套用調整至\n"
"  crtc=N\t串串逗號幅來各別 CRTCs 去套用調整至\n"

#: src/gamma-randr.c:332
#, c-format
msgid "Unable to read screen number: `%s'.\n"
msgstr "無法讀取燭幕數字： `%s'。\n"

#: src/gamma-randr.c:368 src/gamma-vidmode.c:160
#, c-format
msgid ""
"Parameter `%s` is now always on;  Use the `%s` command-line option to "
"disable.\n"
msgstr "參數 `%s` 現在是永遠開啟；  使用 `%s` 指令選項去停用。\n"

#: src/gamma-vidmode.c:65 src/gamma-vidmode.c:85 src/gamma-vidmode.c:94
#: src/gamma-vidmode.c:121 src/gamma-vidmode.c:184 src/gamma-vidmode.c:229
#, c-format
msgid "X request failed: %s\n"
msgstr "X 請求失敗： %s\n"

#: src/gamma-vidmode.c:144
msgid "Adjust gamma ramps with the X VidMode extension.\n"
msgstr "使用 X VidMode extension 去調整咖瑪斜面。\n"

#. TRANSLATORS: VidMode help output
#. left column must not be translated
#: src/gamma-vidmode.c:149
msgid "  screen=N\t\tX screen to apply adjustments to\n"
msgstr "  螢幕=N\t\tX 螢幕去套用調整至\n"

#: src/gamma-dummy.c:47
msgid ""
"WARNING: Using dummy gamma method! Display will not be affected by this "
"gamma method.\n"
msgstr "警示：正在使用虛擬咖瑪方法！顯示器不受此咖瑪方法的影響。\n"

#: src/gamma-dummy.c:64
msgid ""
"Does not affect the display but prints the color temperature to the "
"terminal.\n"
msgstr "不要影響顯示器，但列印色溫至終端器。\n"

#: src/gamma-dummy.c:79
#, c-format
msgid "Temperature: %i\n"
msgstr "色溫： %i\n"

#: src/location-geoclue2.c:63
msgid ""
"Access to the current location was denied by GeoClue!\n"
"Make sure that location services are enabled and that Redshift is permitted\n"
"to use location services. See https://github.com/jonls/redshift#faq for "
"more\n"
"information.\n"
msgstr ""
"取用目前位置資訊遭到 GeoClue 拒絕！\n"
"要確定位置提供服務是啟用的，且允許 Redshift 使用位置服務。\n"
"請見網頁 https://github.com/jonls/redshift#faq 更多資訊。\n"

#: src/location-geoclue2.c:111
#, c-format
msgid "Unable to obtain location: %s.\n"
msgstr "無法取得位置： %s。\n"

#: src/location-geoclue2.c:154
#, c-format
msgid "Unable to obtain GeoClue Manager: %s.\n"
msgstr "無法取得 GeoClue 管理者： %s。\n"

#: src/location-geoclue2.c:170
#, c-format
msgid "Unable to obtain GeoClue client path: %s.\n"
msgstr "無法取得 GeoClue 客戶端路徑： %s。\n"

#: src/location-geoclue2.c:192
#, c-format
msgid "Unable to obtain GeoClue Client: %s.\n"
msgstr "無法取得 GeoClue 客戶端： %s。\n"

#: src/location-geoclue2.c:233
#, c-format
msgid "Unable to set distance threshold: %s.\n"
msgstr "無法設置距離閥值： %s。\n"

#: src/location-geoclue2.c:257
#, c-format
msgid "Unable to start GeoClue client: %s.\n"
msgstr "無法啟動 GeoClue 客戶端： %s。\n"

#: src/location-geoclue2.c:369
msgid "Failed to start GeoClue2 provider!\n"
msgstr "啟動 GeoClue2 供應服務失敗！\n"

#: src/location-geoclue2.c:403
msgid "Use the location as discovered by a GeoClue2 provider.\n"
msgstr "使用位置作為 GeoClue2 供應者所發現的。\n"

#: src/location-manual.c:63
msgid "Latitude and longitude must be set.\n"
msgstr "經度緯度必須設置。\n"

#: src/location-manual.c:79
msgid "Specify location manually.\n"
msgstr "手動指定位置。\n"

#. TRANSLATORS: Manual location help output
#. left column must not be translated
#: src/location-manual.c:84
msgid ""
"  lat=N\t\tLatitude\n"
"  lon=N\t\tLongitude\n"
msgstr ""
"  lat=N\t\t緯度\n"
"  lon=N\t\t經度\n"

#: src/location-manual.c:87
msgid ""
"Both values are expected to be floating point numbers,\n"
"negative values representing west / south, respectively.\n"
msgstr ""
"兩個數均為浮點數字，\n"
"負數分代表西經或南緯。\n"

#: src/location-manual.c:101
msgid "Malformed argument.\n"
msgstr "異常參數\n"

#: src/redshift-gtk/statusicon.py:83
msgid "Suspend for"
msgstr "暫緩執行"

#: src/redshift-gtk/statusicon.py:85
msgid "30 minutes"
msgstr "30 分鐘"

#: src/redshift-gtk/statusicon.py:86
msgid "1 hour"
msgstr "1 小時"

#: src/redshift-gtk/statusicon.py:87
msgid "2 hours"
msgstr "2 小時"

#: src/redshift-gtk/statusicon.py:88
#, fuzzy
msgid "4 hours"
msgstr "2 小時"

#: src/redshift-gtk/statusicon.py:89
#, fuzzy
msgid "8 hours"
msgstr "2 小時"

#: src/redshift-gtk/statusicon.py:98
msgid "Autostart"
msgstr "自動啟動"

#: src/redshift-gtk/statusicon.py:110 src/redshift-gtk/statusicon.py:120
msgid "Info"
msgstr "資訊"

#: src/redshift-gtk/statusicon.py:115
msgid "Quit"
msgstr "停止"

#: src/redshift-gtk/statusicon.py:153
msgid "Close"
msgstr "關閉"

#: src/redshift-gtk/statusicon.py:313
msgid "<b>Status:</b> {}"
msgstr "<b>狀態：</b> {}"

#: src/redshift-gtk/statusicon.py:319 src/redshift-gtk/statusicon.py:337
msgid "Color temperature"
msgstr "色溫"

#: src/redshift-gtk/statusicon.py:325 src/redshift-gtk/statusicon.py:338
msgid "Period"
msgstr "期間"

#: src/redshift-gtk/statusicon.py:331
msgid "Location"
msgstr "位置"

#: src/redshift-gtk/statusicon.py:362
msgid "Please run `redshift -h` for help output."
msgstr "請執行 `redshift -h` 獲得說明輸出。"

#~ msgid ""
#~ "Redshift adjusts the color temperature of your screen according to your "
#~ "surroundings. This may help your eyes hurt less if you are working in "
#~ "front of the screen at night."
#~ msgstr ""
#~ "Redshift 根據你周圍環境來調節整螢幕色溫。\n"
#~ "如果夜晚在螢幕前工作，這可以減少眼睛受到傷害。"

#~ msgid ""
#~ "The color temperature is set according to the position of the sun. A "
#~ "different color temperature is set during night and daytime. During "
#~ "twilight and early morning, the color temperature transitions smoothly "
#~ "from night to daytime temperature to allow your eyes to slowly adapt."
#~ msgstr ""
#~ "根據太陽位置日夜變化來設置螢幕色溫。隨著日夜交替時間轉換，色溫設置則會逐漸"
#~ "變化，可讓您的眼睛能夠漸漸地適應。"

#~ msgid ""
#~ "This program provides a status icon that allows the user to control "
#~ "Redshift."
#~ msgstr "軟體提供狀態圖示，讓使用者來控制 Redshift 。"

#~ msgid ""
#~ "The Redshift information window overlaid with an example of the redness "
#~ "effect"
#~ msgstr "Redshift 資訊視窗鋪上一層紅暈效果的的示範"

#~ msgid "Color temperature adjustment"
#~ msgstr "色溫調整"

#~ msgid "Color temperature adjustment tool"
#~ msgstr "色溫調整工具"

#~ msgid "Unable to save current gamma ramp.\n"
#~ msgstr "無法儲存目前的咖瑪斜面。\n"

#~ msgid "Adjust gamma ramps on macOS using Quartz.\n"
#~ msgstr "在 macOS 使用 Quarts 來調整咖瑪斜面。\n"

#~ msgid "Unable to open device context.\n"
#~ msgstr "無法開啟設備前後內文。\n"

#~ msgid "Display device does not support gamma ramps.\n"
#~ msgstr "顯示器沒有支援咖瑪斜面。\n"

#~ msgid "Unable to restore gamma ramps.\n"
#~ msgstr "無法存入咖瑪斜面。\n"

#~ msgid "Unable to set gamma ramps.\n"
#~ msgstr "無法設置咖瑪斜面。\n"

#~ msgid "Not authorized to obtain location from CoreLocation.\n"
#~ msgstr "來自 CoreLocation 取得位置資訊未獲授權。\n"

#, c-format
#~ msgid "Error obtaining location from CoreLocation: %s\n"
#~ msgstr "來自 CoreLocation 取得位置資訊錯誤：%s\n"

#~ msgid "Waiting for authorization to obtain location...\n"
#~ msgstr "等待授權以取得位置資訊...\n"

#~ msgid "Request for location was not authorized!\n"
#~ msgstr "請求位置尚未授權！\n"

#~ msgid "Failed to start CoreLocation provider!\n"
#~ msgstr "啟動 CoreLocation 供應服務失敗！\n"

#~ msgid "Use the location as discovered by the Corelocation provider.\n"
#~ msgstr "使用位置作為 Corelocation 供應者所發現的.\n"
